object msort extends App {

  val generator = scala.util.Random
  var unsortedVector = Vector[Int]()


  println ("Unsorted vector: " + {

    // проверки
    //for (i <- 0 to 10)
    //unsortedVector = Vector (1, 2, 3, 4, 20, 9, 90, 41, 39, 92, 11)
    //unsortedVector = Vector (10, 12, 33, 24, 20, 9, 90, 4, 3, 2, 1)
    //unsortedVector = Vector (10, 12, 33, 24, 20, 9, 90, 10, 10, 1, 1)

    // создаем и заполняем вектор случайными числами [0:99]
    for (i <- 0 to generator.nextInt(30))
      unsortedVector = unsortedVector :+ generator.nextInt(100)
    unsortedVector
  })

  println ("Sorted vector: " +
    mergeSort(unsortedVector))




  def mergeSort (targetVector: Vector[Int]): Vector[Int] = {

    devideAndSort (targetVector, 0, targetVector.length-1)

  }

  def devideAndSort (vec: Vector[Int], lIndex:Int, uIndex:Int): Vector[Int] = {


    if (uIndex - lIndex > 0) {

      val left = lIndex
      val right = uIndex
      val mid = lIndex + (uIndex - lIndex) / 2

      // разделяем части вектора
      val part1 = devideAndSort (vec, left, mid)
      val part2 = devideAndSort (vec, mid+1, right)

      // объединяем части вектора
      merge(part1, part2)


    } else
    // спустились до дна вектора, часть состоит из одного элемента
      vec.slice(lIndex,uIndex + 1)

  }

  def merge (p1: Vector[Int], p2: Vector[Int]): Vector[Int] = {

    var part1Pos1 = 0
    var part2Pos1 = 0

    var partsMerged = Vector[Int]()

    //print (s"$p1 \n$p2 ")

    // проходи по индексам пары значений двух векторов, сортируем пары, укладываем в итоговый вектор
    while (part1Pos1 < p1.length && part2Pos1 < p2.length) {
      if (p1(part1Pos1) < p2(part2Pos1)){
        partsMerged = partsMerged :+ p1(part1Pos1)
        part1Pos1 += 1
      } else {
        partsMerged = partsMerged :+ p2(part2Pos1)
        part2Pos1 += 1
      }
    }

    // если какая-то из частей оказалась больше, то дописываем ее в конец
    while (part1Pos1 < p1.length) {
      partsMerged = partsMerged :+ p1(part1Pos1)
      part1Pos1 += 1
    }
    while (part2Pos1 < p2.length) {
      partsMerged = partsMerged :+ p2(part2Pos1)
      part2Pos1 += 1
    }

    //println (s"merged in $partsMerged")

    partsMerged
  }

}

