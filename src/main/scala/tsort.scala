object tsort extends App {

  val generator = scala.util.Random
  var inVector = Vector[Int]()

  //print("Type N: ")
  //val n = scala.io.StdIn.readInt()
  //println("N: " + n)

  println ("Input: " + {

    // проверки
    //for (i <- 0 to 10)
    //inVector = Vector (1, 2, 3, 4, 20, 9, 90, 41, 39, 92, 11)
    //inVector = Vector (91, 90, 33, 24, 20, 9, 90, 4, 3, 2, 1)
    //inVector = Vector(82, 94, 66, 8, 89, 15, 78, 74, 4, 65, 20, 56, 88, 37, 13)
    //inVector = Vector ()
    //inVector = Vector (37,64,86)
    //inVector = Vector (11,9)

    // создаем и заполняем вектор случайными числами [0:99]
    for (i <- 0 to generator.nextInt(30))
      inVector = inVector :+ generator.nextInt(100)
    inVector
  })

  println ("Top-N: " + {
    topN(inVector, 7)}
  )



  def topN (vIn: Vector [Int], n: Int): Vector [Int] = {

    var vOut = Vector[Int]()

    for (inElem <- vIn) {

      // позиция в векторе с топ значениями
      var pos = 0

      // определяем позицию вставки значения
      for (k <- 0 until vOut.length) {
        if (inElem >= vOut(k) && k < n) {
          pos += 1
        }
      }

      // вставляем значение в вектор с топ значениями
      if (vOut.length == n) { // вектор с топом уже заполнен
        if (pos == 0) {
          vOut = (inElem +: vOut).take(n)
        } else if (pos > 0 && pos < n) {
          vOut = (vOut.slice(0,pos) :+ inElem :++ vOut.slice(pos,vOut.length+1)).take(n)
        } else {
          // явно больше чем уже имеющийся топ
        }

      } else { // вектор с топом еще не заполнен
        if (pos == 0) {
          vOut = inElem +: vOut
        } else if (pos > 0 && pos < vOut.length) {
          vOut = vOut.slice(0,pos) :+ inElem :++ vOut.slice(pos,vOut.length+1)
        } else {
          vOut = vOut :+ inElem
        }
      }

    }

    vOut
  }

}